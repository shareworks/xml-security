XmlSecurity
===========

The XmlSecurity library provides PHP support for the XML Security extensions (encryption and signatures).
It is a port of [xmlseclibs](https://code.google.com/p/xmlseclibs/) to provide a PSR-0 compatible structure.

To do
-----
Port tests from original library to PhpUnit